'use strict';

/**
 * @ngdoc overview
 * @name swtestApp
 * @description
 * # swtestApp
 *
 * Main module of the application.
 */
angular
  .module('swtestApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'LocalStorageModule',
    'angular-md5',
    'ui.bootstrap'
  ])

  .run(function ($rootScope, localStorageService, md5) {

    // simple authorization part for this demo
    // init users from localStorage
    $rootScope.users = localStorageService.get('users');
    if (!$rootScope.users) {
      $rootScope.users = [];
    }
    // get authorized user from local storage
    $rootScope.authorized = false;
    $rootScope.authorizedUser = localStorageService.get('authorizedUser');
    if ($rootScope.authorizedUser) {
      $rootScope.authorized = true;
    }

    $rootScope.hashPassword = function(password) {
      var hash = 0;
      if (password && password.length === 0) return hash;
      hash = md5.createHash(password);
      return hash;
    };

    $rootScope.signupNewUser = function (email, password) {
      if ($rootScope.users) {
        var userExists = false;
        for (var i = 0; i < $rootScope.users.length; i++) {
          if ($rootScope.users[i] && $rootScope.users[i].email && $rootScope.users[i].email === email) {
            userExists = true;
          }
        }
        if (userExists) {
          alert("User with such email already exists.");
        } else {
          var newUser = {};
          newUser.email = email;
          newUser.password = $rootScope.hashPassword(password);
          $rootScope.users.push(newUser);
          localStorageService.set('users', $rootScope.users);
        }
      }
    };

    $rootScope.loginUser = function (email, password) {
      if ($rootScope.users) {
        var userExists = false;
        var foundUser;
        for (var i = 0; i < $rootScope.users.length; i++) {
          if ($rootScope.users[i] && $rootScope.users[i].email && $rootScope.users[i].email === email) {
            userExists = true;
            foundUser = $rootScope.users[i];
            break;
          }
        }
        if (userExists && foundUser) {
          var hashedPassword = $rootScope.hashPassword(password);
          if (foundUser.password === hashedPassword) {
            $rootScope.authorized = true;
            $rootScope.authorizedUser = foundUser;
            localStorageService.remove('authorizedUser');
            localStorageService.set('authorizedUser', foundUser);
          } else {
            alert("Wrong password.");
          }
        } else {
          alert("User with such email doesn't exist.");
        }
      }
    };

    $rootScope.logoutUser = function () {
      $rootScope.authorized = undefined;
      $rootScope.authorizedUser = undefined;
      localStorageService.remove('authorizedUser');
    };

  })

  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/signup', {
        templateUrl: 'views/signup.html',
        controller: 'SignupCtrl',
        controllerAs: 'signup'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
