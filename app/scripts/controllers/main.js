'use strict';

/**
 * @ngdoc function
 * @name swtestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the swtestApp
 */
angular.module('swtestApp')
  .controller('MainCtrl', function ($scope, $rootScope, $http, localStorageService) {

    $scope.init = function () {
      // init colors
      $scope.colors = ["Red","White","Black","Blue","Yellow","Green"];

      // init filter
      $scope.clearFilter();

      //load items
      $scope.items = [];
      $http.get('data/items.txt').then(function (response) {
        //console.log(response.data);
        $scope.items = response.data;
      });
    };

    $scope.clearFilter = function () {
      $scope.filter = {};
      $scope.filter.inStockOnly = false;
      $scope.filter.color = "all";
      $scope.filter.dateFrom = undefined;
      $scope.filter.dateTo = undefined;
    };

    $scope.orderItem = function (id) {
      var orderedItem = undefined;
      for (var i = 0; i < $scope.items.length; i++) {
        if ($scope.items[i] && $scope.items[i].id && $scope.items[i].id === id) {
          orderedItem = $scope.items[i];
        }
      }
      if (orderedItem && $rootScope.userCartItems) {
        var isAlreadyInCart = false;
        for (var i = 0; i < $rootScope.userCartItems.length; i++) {
          if ($rootScope.userCartItems[i] && $rootScope.userCartItems[i].id && $rootScope.userCartItems[i].id === orderedItem.id) {
            isAlreadyInCart = true;
          }
        }
        if (isAlreadyInCart) {
          alert("You have already added this item to your Cart");
        } else {
          $rootScope.userCartItems.push(orderedItem);
          localStorageService.set('userCartItems', $rootScope.userCartItems);
        }
      }
    };

    $scope.init();
  })

  .filter('filterItemsByIssueDates', function () {
    return function (items, dateFrom, dateTo) {
      var filtered = [];
      var df = new Date(dateFrom);
      var dt = new Date(dateTo);

      angular.forEach(items, function (item) {
        var itemDate = new Date(item.issueDate);
        //console.log(itemDate.getTime());
        //console.log(df.getTime());
        //console.log(dt.getTime());
        if (angular.isDate(itemDate) && !isNaN(df.getTime()) && !isNaN(dt.getTime())) {
          if (itemDate >= df && itemDate <= dt) {
            filtered.push(item);
          }
        } else if (angular.isDate(itemDate) && !isNaN(df.getTime())) {
          if (itemDate >= df) {
            filtered.push(item);
          }
        } else if (angular.isDate(itemDate) && !isNaN(dt.getTime())) {
          if (itemDate <= dt) {
            filtered.push(item);
          }
        } else {
          filtered.push(item);
        }
      });
      return filtered;
    };
  })
  .filter('filterItemsByPrices', function () {
    return function (items, priceFrom, priceTo) {
      var filtered = [];
      var pf = parseFloat(priceFrom);
      var pt = parseFloat(priceTo);
      angular.forEach(items, function (item) {
        var itemPrice = parseFloat(item.price);
        if (!isNaN(itemPrice) && !isNaN(pf) && !isNaN(pt)) {
          if (itemPrice >= pf && itemPrice <= pt) {
            filtered.push(item);
          }
        } else if (!isNaN(itemPrice) && !isNaN(pf)) {
          if (itemPrice >= pf) {
            filtered.push(item);
          }
        } else if (!isNaN(itemPrice) && !isNaN(pt)) {
          if (itemPrice <= pt) {
            filtered.push(item);
          }
        } else {
          filtered.push(item);
        }
      });
      return filtered;
    };
  })
  .filter('filterItemsByInStockOnly', function () {
    return function (items, inStockOnly) {
      var filtered = [];
      angular.forEach(items, function (item) {
        if (inStockOnly) {
          if (item.inStock) {
            filtered.push(item);
          }
        } else {
          filtered.push(item);
        }
      });
      return filtered;
    };
  })
  .filter('filterItemsByColor', function () {
    return function (items, color) {
      var filtered = [];
      angular.forEach(items, function (item) {
        if (color !== 'all') {
          if (color == item.color ) {
            filtered.push(item);
          }
        } else {
          filtered.push(item);
        }
      });
      return filtered;
    };
  });
