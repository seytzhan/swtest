'use strict';

/**
 * @ngdoc function
 * @name swtestApp.controller:SignupCtrl
 * @description
 * # SignupCtrl
 * Controller of the swtestApp
 */
angular.module('swtestApp')
  .controller('SignupCtrl', function ($scope, $rootScope) {

    $scope.newAccount = {};

    $scope.submit = function () {

        if ($scope.form.$valid) {
          $scope.submitting = true;
          $scope.passwordsDoNotMatch = null;
          $scope.errorEmailExists = null;

          if ($scope.newAccount.password !== $scope.newAccount.confirmPassword) {
            $scope.passwordsDoNotMatch = 'ERROR';
            $scope.submitting = false;
          } else {
            $rootScope.signupNewUser($scope.newAccount.email, $scope.newAccount.password);

            // clear
            $scope.newAccount = {};
            $scope.submitting = false;
          }

        }

    };

  });
