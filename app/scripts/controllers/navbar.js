'use strict';

/**
 * @ngdoc function
 * @name swtestApp.controller:NavbarCtrl
 * @description
 * # NavbarCtrl
 * Controller of the swtestApp
 */
angular.module('swtestApp')
  .controller('NavbarCtrl', function ($scope, $rootScope, localStorageService, $uibModal) {

    $scope.init = function () {
      // init user Cart
      $scope.isUsercartOpen = false;
      $rootScope.userCartItems = localStorageService.get('userCartItems');
      if (!$rootScope.userCartItems) {
        $rootScope.userCartItems = [];
      }
    };

    $scope.openLoginModal = function () {
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'loginModal.html',
        controller: 'LoginModalCtrl'
      });

      modalInstance.result.then(function (loginAccount) {
        $rootScope.loginUser(loginAccount.email, loginAccount.password);
      }, function () {
      });
    };

    $scope.toggleUserCart = function () {
      $scope.isUsercartOpen = !$scope.isUsercartOpen;
    };

    $scope.removeItemFromUserCart = function (id) {
      for (var i = 0; i < $rootScope.userCartItems.length; i++) {
        if ($rootScope.userCartItems[i] && $rootScope.userCartItems[i].id && $rootScope.userCartItems[i].id === id) {
          $rootScope.userCartItems.splice(i, 1);
        }
      }
      localStorageService.set('userCartItems', $rootScope.userCartItems);
    };

    $scope.init();
  })
  .controller('LoginModalCtrl', function ($scope, $uibModalInstance) {

    $scope.loginAccount = {};

    $scope.login = function () {
      $uibModalInstance.close($scope.loginAccount);
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });;
