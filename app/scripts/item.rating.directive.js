'use strict';

/**
 * @ngdoc function
 * @name swtestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the swtestApp
 */
angular.module('swtestApp')
  .directive('itemRating', function () {

    return {
      restrict: 'EA',
      template: '<div class="item_rating">'
        + ' <i class="glyphicon" ng-repeat="star in stars" ng-class="star.className"></i>'
        + '</div>',
      scope: {
        ratingValue: '=ngModel'
      },
      link: function (scope, elem, attrs) {
        var showRating = function () {
          scope.stars = [];
          for (var i = 0; i < 5; i++) {
            var active = i < scope.ratingValue;
            var className = "glyphicon-star-empty";
            if (active) {
              className = "glyphicon-star";
            }
            scope.stars.push({
              active: active,
              className: className
            });
          }
        };
        scope.$watch('ratingValue',
          function (oldVal, newVal) {
            showRating();
          }
        );
      }
    };
  });
